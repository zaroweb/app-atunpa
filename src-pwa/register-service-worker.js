import { register } from "register-service-worker";

const serviceWorkerFile = "/sw.js"; // Asegúrate de que esta ruta sea correcta

register(serviceWorkerFile, {
  ready(registration) {
    console.log("Service worker is active.", registration);
  },
  registered(registration) {
    console.log("Service worker has been registered.", registration);
  },
  cached(registration) {
    console.log("Content has been cached for offline use.", registration);
  },
  updatefound(registration) {
    console.log("New content is downloading.", registration);
  },
  updated(registration) {
    console.log("New content is available; please refresh.", registration);
  },
  offline() {
    console.log(
      "No internet connection found. App is running in offline mode."
    );
  },
  error(err) {
    console.error("Error during service worker registration:", err);
  },
});
